> **NOTE:** This README.md file should eb placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Zachary Birchall

### Class Number Requirements

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Install MYSQL and AMMPS
    - Provide screenshots of installations
    - Provide screenshots of database and data
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    

2. [A2 README.md](A2/README.md "My A2 README.md file")
    - Use MYSQL and AMPPS
    - Create database using MYSQL
    - Hash/salt important information
    - Provide screenshots
    - Provide git command descriptions

3. [A3 README.md](A3/README.md "My A3  README.md file")
    - Database using MYSQL 
    - Provide screenshots of SQL code and populated tables(in Oracle environment)
    - Push local files to Bitbucket repo
    - Create README

4. [P1 README.md](P1/README.md "My P1 README.md file")
    - Database using SQL code
    - Provide screenshots of ERD and populated tables
    - Push local files to Bitbucket repo
    - Create README
   

5. [A4 README.md](A4/README.md "My A4 README.md file")
    - Database in Microsoft SQL Server 
    - Provide Screenshots of ERD and populated tables
    - Push local files to Bitbucket repo
    - Create REAMDME
    

6. [A5 README.md](A5/README.md "My A5 README.md file")
    - Database in Microsoft SQL Server 
    - Provide Screenshots of ERD and newly added populated tables
    - Push local files to Bitbucket repo
    - Create REAMDME
    

7. [P2 README.md](P2/README.md "My P2 README.md file")
   