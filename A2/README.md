# LIS 3781

## Zachary Birchall

### Assignment 2 Requirements:

1. Install MYSQL and AMMPS
2. Database using MYSQL 
3. Provide screenshots of code and populated tables
4. Push local files to Bitbucket repo
5. Create README

#### README.md file should include the following items:

* Assignment Screenshots
* Git commands

#### Git commands w/ short descriptions:
1. git init - initialize a local Git repository
2. git add - add all new and changed files to staging
3. git commit - commit changes
4. git push - push changes to remote repository
5. git pull - update lcoal changes to the newest repository
6. git status - checks status
7. git branch - list branches


##### Assignment Screenshots:

*Screenshots of SQL code*:

![Part 1](img/a2code1.png)| ![Part 2](img/a2code2.png)

*Screenshot of data-filled tables*:

![Data-Filled Tables](img/data_filled_tables.png)
