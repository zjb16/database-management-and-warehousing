# LIS 3781

## Zachary Birchall

### Project 1 Requirements:

1. Database using MYSQL 
2. Provide screenshots of SQL code and populated tables
3. Push local files to Bitbucket repo
4. Create README

#### README.md file should include the following items:

* Assignment Screenshots


##### Assignment Screenshots:

*Screenshots of Entity Relational Diagram*:

![ERD](img/p1_ERD.png)

*Screenshot of data-filled tables*:

![Data-Filled Tables](img/p1_populated_tables.PNG)
