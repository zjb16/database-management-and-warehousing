# LIS 3781

## Zachary Birchall

### Assignment 3 Requirements:

1. Database using MYSQL 
2. Provide screenshots of SQL code and populated tables(in Oracle environment)
3. Push local files to Bitbucket repo
4. Create README

#### README.md file should include the following items:

* Assignment Screenshots


##### Assignment Screenshots:

*Screenshots of SQL code*:

![SQL CODE](img/a3_code.PNG)

*Screenshot of data-filled tables*:

![Data-Filled Tables](img/a3_populated_tables.PNG)
