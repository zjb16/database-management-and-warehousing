# LIS 3781

## Zachary Birchall

### Assignment 5 Requirements:

1. Database using Microsoft SQL Server 
2. Provide screenshots of newly added populated tables
3. Push local files to Bitbucket repo
4. Create README

#### README.md file should include the following items:

* Assignment Screenshots


##### Assignment Screenshots:


*Screenshots of data-filled tables*:
![Data-Filled Tables](img/region_state_city.PNG)
![Data-Filled Tables](img/time_sale.PNG)

*Screenshot of ERD*:
![ERD](img/a5_erd.PNG)