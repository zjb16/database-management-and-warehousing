# LIS 3781

## Zachary Birchall

### Assignment 1 Requirements:

1. Install MYSQL and AMMPS
2. Create database using MYSQL 
3. Provide Screenshots of tables and data
4. Push local files to Bitbucket repo
5. Create README

#### README.md file should include the following items:
* Git commands

#### Git commands w/ short descriptions:
1. git init - initialize a local Git repository
2. git add - add all new and changed files to staging
3. git commit - commit changes
4. git push - push changes to remote repository
5. git pull - update lcoal changes to the newest repository
6. git status - checks status
7. git branch - list branches


##### Assignment Screenshots:

*Screenshot of database*:

![Database](img/a1_erd.PNG)

*Screenshot of data-filled tables*:

![Data-filled Tables](img/a1_populated_tables.PNG)
