# LIS 3781

## Zachary Birchall

### Assignment 4 Requirements:

1. Database using Microsoft SQL Server 
2. Provide screenshots of SQL code and populated tables
3. Push local files to Bitbucket repo
4. Create README

#### README.md file should include the following items:

* Assignment Screenshots


##### Assignment Screenshots:

*Screenshots of SQL code*:

![SQL CODE](img/a4_sql_code.PNG)

*Screenshot of data-filled tables*:

![Data-Filled Tables](img/successful_data.PNG)

*Screenshot of ERD*:
![ERD](img/a4_erd.PNG)